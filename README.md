# Software Studio 2018 Spring Assignment 02 小朋友下樓梯

## 小朋友下樓梯
<img src="example01.png" width="700px" height="500px"></img>

## Goal
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Complete a game "小朋友下樓梯" by Phaser. (JavaScript or TypeScript)
3. Your game should reach the basic requirements.
4. You can download needed materials from some open source webpage to beautify the appearance.
5. Commit to "your" project repository and deploy to Gitlab page.
6. **Report which items you have done and describing other functions or feature in REABME.md.**

## Scoring 
|                                              Item                                              | Score |
|:----------------------------------------------------------------------------------------------:|:-----:|
| A complete game process: start menu => game view => game over => quit or play again            |  20%  |
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  15%  |
|         All things in your game should have correct physical properties and behaviors.         |  15%  |
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  10%  |
| Add some additional sound effects and UI to enrich your game.                                  |  10%  |
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  10%  |
| Appearance (subjective)                                                                        |  10%  |
| Other creative features in your game (describe on README.md)                                   |  10%  |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/Assignment_02**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, .ts, etc.
    * source files
* **Deadline: 2018/05/24 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

## My Report About What Function I've Done
|                                              Item                                              | Score |
|:----------------------------------------------------------------------------------------------:|:-----:|
| Loading頁面結束後會進入選單根據上方文字用**滑鼠**點按可以進入對應畫面分別是1P,2P遊戲畫面,遊戲說明跟1P排行榜,Game Over之後會回到選單,說明跟排行榜都有Quit鍵可以回到選單|  20%  |
| 基本上跟小朋友下樓梯規則一樣,碰到尖刺會扣血,血量歸零會掉出畫面玩家都會死亡                                 |  15%  |
|         假階梯,輸送帶跟彈簧分別有各自的物理效果         |  15%  |
| 我有做假階梯,輸送帶跟彈簧 |  10%  |
|玩遊戲會有背景音樂跟到尖刺會發出尖叫聲,碰觸尖刺會有Camera Flash效果,人物死亡會粉碎跟畫面震動                                  |  10%  |
| 一開始會要求輸入名字,玩一次1P之後若超過前十高分進入1P排行榜即可看到自己的排名        |  10%  |
| 遊戲主題簡單風,看起來十分有格調                                                                        |  10%  |
| 提供2P對戰系統|  10%  |
 
 **如果網頁有什麼問題,請助教一定要告訴我,發現好像在某些電腦跑會沒音樂,不知是不是瀏覽器還是gitlab的server問題**