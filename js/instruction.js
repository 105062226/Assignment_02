var inst;
var quit;
var instrucState={
    create: function() {
        inst = game.add.sprite(game.width/2-50,200, 'instruction');
        inst.anchor.setTo(0.5, 0.5); 
        inst.scale.setTo(0.5,0.5);
        quit= game.add.text(420,400,"Quit", { font: '25px Arial', fill: '#ffffff' });
        quit.anchor.setTo(0.5, 0.5);
        quit.inputEnabled = true;        
        quit.events.onInputDown.add(quitdown, this);
        quit.events.onInputOver.add(over, this);
        quit.events.onInputOut.add(out, this);
    }
}

function quitdown (item) {
    game.state.start('menu');
}
