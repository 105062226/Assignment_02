var player;
var player2;
var keyboard;
var platforms = [];
var leftWall;
var rightWall;
var ceiling;
var text1;
var text2;
var text3;
var distance = 0;
var emitter;
var playerdie=false;
var player2die=false;
var main2State = {
    preload: function() {
        distance=0;
        playerdie=false;
        player2die=false;
        this.scale.pageAlignHorizontally = true;
        game.load.crossOrigin = 'anonymous';
        diesound=game.add.audio('die');
        gamebgm=game.add.audio('gamebgm');
        gamebgm.loop=true;
        hurtsound=game.add.audio('hurt');
    },

    create: function() {
        game.stage.backgroundColor = '#3498db'; 
        keyboard = game.input.keyboard.addKeys({
            'enter': Phaser.Keyboard.ENTER,
            'up': Phaser.Keyboard.UP,
            'down': Phaser.Keyboard.DOWN,
            'left': Phaser.Keyboard.LEFT,
            'right': Phaser.Keyboard.RIGHT,
            'w': Phaser.Keyboard.W,
            'a': Phaser.Keyboard.A,
            's': Phaser.Keyboard.S,
            'd': Phaser.Keyboard.D
        });
        gamebgm.play();
        createPlatforms();
        createHealthBar()
        createHealthBar2()
        createEmitter();
        createBounders();
        createPlayer();
        createPlayer2();
        createTextsBoard();
    },
    update: function() {
        this.physics.arcade.collide(player, platforms, effect);
        this.physics.arcade.collide(player, [leftWall, rightWall]);
        this.physics.arcade.collide(player, ceiling,checkTouchCeiling);
        this.physics.arcade.collide(player2, platforms, effect);
        this.physics.arcade.collide(player2, [leftWall, rightWall]);
        this.physics.arcade.collide(player2, ceiling,checkTouchCeiling);
        this.physics.arcade.collide(player2, player);
        checkdie();
        checkdie2();      
        updateHealthBar();
        updateHealthBar2();
        updatePlayer();
        updatePlayer2();
        updatePlatforms();
        updateTextsBoard();
        createPlatforms();
        text1.setText("P1 :");
        text3.setText("P2 :")
    }
};

function createEmitter(){
        emitter = game.add.emitter(422, 320, 15);
        emitter.makeParticles('pixel');
        emitter.setYSpeed(-150, 150);
        emitter.setXSpeed(-150, 150);
        emitter.setScale(2, 0, 2, 0, 700);
        emitter.gravity =0;
}

function createPlayer() {
    player = game.add.sprite(200, 10, 'player');
    player.direction = 10;
    game.physics.arcade.enable(player);
    player.body.gravity.y = 400;
    player.animations.add('left', [3,4], 8);
    player.animations.add('right', [1,2], 8);
    player.animations.add('flyleft', [7,8], 12);
    player.animations.add('flyright', [5,6], 12);
    player.animations.add('fly', [0], 12);
    player.life = 10;
    player.unbeatableTime = 0;
    player.touchOn = undefined;
}

function createPlayer2() {
    player2 = game.add.sprite(250, 20, 'player2');
    player2.direction = 10;
    game.physics.arcade.enable(player2);
    player2.body.gravity.y = 400;
    player2.animations.add('left', [3,4], 8);
    player2.animations.add('right', [1,2], 8);
    player2.animations.add('flyleft', [7,8], 12);
    player2.animations.add('flyright', [5,6], 12);
    player2.animations.add('fly', [0], 12);
    player2.life = 10;
    player2.unbeatableTime = 0;
    player2.touchOn = undefined;
}

function myCallback(){
    console.log("in");
}


function createBounders () {
    leftWall = game.add.sprite(0, 0, 'wall');
    game.physics.arcade.enable(leftWall);
    leftWall.body.immovable = true;

    rightWall = game.add.sprite(449, 0, 'wall');
    game.physics.arcade.enable(rightWall);
    rightWall.body.immovable = true;

    ceiling = game.add.sprite(52, 0, 'ceiling');
    game.physics.arcade.enable(ceiling);
    ceiling.body.immovable = true;
}

var lastTime = 0;

function createPlatforms () {
    if(game.time.now > lastTime + 750) {
        lastTime = game.time.now;
        createOnePlatform();
        distance += 1;
    }
}

function createOnePlatform () {

    var platform;
    var x =  Math.random()*(500 - 96 - 40);
    var y = 450;

    if(x<=55) x=55;
    else if(x>=350)x=350;
    var rand = Math.random() * 100;
    if(rand < 20) {
        platform = game.add.sprite(x, y, 'normal');
    } else if (rand < 40) {
        platform = game.add.sprite(x, y, 'nails');
        game.physics.arcade.enable(platform);
        platform.body.setSize(96, 15, 0, 15);
    } else if (rand < 50) {
        platform = game.add.sprite(x, y, 'conveyorLeft');
        platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
        platform.play('scroll');
    } else if (rand < 60) {
        platform = game.add.sprite(x, y, 'conveyorRight');
        platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
        platform.play('scroll');
    } else if (rand < 80) {
        platform = game.add.sprite(x, y, 'trampoline');
        platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
        platform.frame = 3;
    } else {
        platform = game.add.sprite(x, y, 'fake');
        platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
    }

    game.physics.arcade.enable(platform);
    platform.body.immovable = true;
    platforms.push(platform);
    platform.body.checkCollision.down = false;
    platform.body.checkCollision.left = false;
    platform.body.checkCollision.right = false;
}

function createTextsBoard () {
    var style = {fill: '#ff0000', fontSize: '20px'}
    text1 = game.add.text(55, 15, '', style);
    text2 = game.add.text(410, 10, '', style);
    text3 = game.add.text(100,100 , '', style);
}

function updatePlayer () {
    if(keyboard.left.isDown) {
        player.body.velocity.x = -250;
    } else if(keyboard.right.isDown) {
        player.body.velocity.x = 250;
    } else {
        player.body.velocity.x = 0;
    }
    setPlayerAnimate(player);
}
function updatePlayer2 () {
    if(keyboard.a.isDown) {
        player2.body.velocity.x = -250;
    } else if(keyboard.d.isDown) {
        player2.body.velocity.x = 250;
    } else {
        player2.body.velocity.x = 0;
    }
    setPlayerAnimate(player2);
}


function setPlayerAnimate(player) {
    var x = player.body.velocity.x;
    var y = player.body.velocity.y;

    if (x < 0 && y > 0) {
        player.animations.play('flyleft');
    }
    if (x > 0 && y > 0) {
        player.animations.play('flyright');
    }
    if (x < 0 && y == 0) {
        player.animations.play('left');
    }
    if (x > 0 && y == 0) {
        player.animations.play('right');
    }
    if (x == 0){
        player.animations.play('fly');
    }
}

function updatePlatforms () {
    for(var i=0; i<platforms.length; i++) {
        var platform = platforms[i];
        platform.body.position.y -= 2;
        if(platform.body.position.y <= -20) {
            platform.destroy();
            platforms.splice(i, 1);
        }
    }
}

function updateTextsBoard () {
    text1.setText('P1-life:');
    text2.setText('B' + distance);
    text3.setText('P2-life:');
}

function effect(player, platform) {
    if(platform.key == 'conveyorRight') {
        conveyorRightEffect(player, platform);
    }
    if(platform.key == 'conveyorLeft') {
        conveyorLeftEffect(player, platform);
    }
    if(platform.key == 'trampoline') {
        trampolineEffect(player, platform);
    }
    if(platform.key == 'nails') {
        nailsEffect(player, platform);
    }
    if(platform.key == 'normal') {
        basicEffect(player, platform);
    }
    if(platform.key == 'fake') {
        fakeEffect(player, platform);
    }
}

function conveyorRightEffect(player, platform) {    
    player.body.x += 2;
}

function conveyorLeftEffect(player, platform) {
    player.body.x -= 2;
}

function trampolineEffect(player, platform) {
    platform.animations.play('jump');
    if(player.life<10)
    {
        player.life+=1;
    }
    
    player.body.velocity.y = -225;
}

function nailsEffect(player, platform) {
    if (player.touchOn !== platform) {
        if(player.life-3<0) player.life=0;
        else {
            player.life -= 3;
            hurtsound.play();
        }
        player.touchOn = platform;
        game.camera.flash(0xff0000, 100);
    }
}

function basicEffect(player, platform) {
    if (player.touchOn !== platform) {
        if(player.life < 10) {
            player.life += 1;    
        }
        player.touchOn = platform;
    }
}

function fakeEffect(player, platform) {
    if(player.touchOn !== platform) {
        platform.animations.play('turn');
        setTimeout(function() {
            platform.body.checkCollision.up = false;
        }, 100);
        player.touchOn = platform;
    }
}

function checkTouchCeiling(player) {
    if(player.life-3<0) player.life=0;
    else {
        player.life-=3;
        hurtsound.play();
    }
    game.camera.flash(0xff0000, 100);
}
var time=0;
function checkdie(){
    if(!player.inWorld||player.life<=0){
        player.kill();
        player.life=0;
        if(game.time.now>time){
            emitter.x = player.x;
            emitter.y = player.y;
            emitter.start(true, 800, null, 15);
            game.camera.shake(0.02, 300);
            diesound.play();
        }
        time=game.time.now+1000;
        playerdie=true;
    }
    if((playerdie==true)&&(player2die==true)) {
        gameOver();
    }
}
var time1=0;

function checkdie2(){
    if(!player2.inWorld||player2.life<=0){
        player2.kill();
        player2.life=0;
        if(game.time.now>time1){
            emitter.x = player2.x;
            emitter.y = player2.y;
            emitter.start(true, 800, null, 15);
            game.camera.shake(0.02, 300);
            diesound.play();
        }      
        time1=game.time.now+1000;
        player2die=true; 
    }
    if((playerdie==true)&&(player2die==true)) {
        gameOver();
    }
}

function gameOver () {
    platforms.forEach(function(s) {s.destroy()});
    platforms = [];
    gamebgm.destroy();
    game.time.events.add(1000,function() {game.state.start('menu');},this);    
}

function createHealthBar() {
 
    meters = game.add.group();
 
    // create a plain black rectangle to use as the background of a health meter
    var meterBackgroundBitmap = game.add.bitmapData(100, 20);
    meterBackgroundBitmap.ctx.beginPath();
    meterBackgroundBitmap.ctx.rect(0, 0, meterBackgroundBitmap.width, meterBackgroundBitmap.height);
    meterBackgroundBitmap.ctx.fillStyle = '#000000';
    meterBackgroundBitmap.ctx.fill();
 
    // create a Sprite using the background bitmap data
    var healthMeterBG = game.add.sprite(95,15, meterBackgroundBitmap);
    healthMeterBG.fixedToCamera = true;
    meters.add(healthMeterBG);
 
    // create a red rectangle to use as the health meter itself
    var healthBitmap = game.add.bitmapData(92, 12);
    healthBitmap.ctx.beginPath();
    healthBitmap.ctx.rect(0, 0, healthBitmap.width, healthBitmap.height);
    healthBitmap.ctx.fillStyle = '#FF0000';
    healthBitmap.ctx.fill();
    // create the health Sprite using the red rectangle bitmap data
    health = game.add.sprite(100, 19, healthBitmap);
    meters.add(health);
    health.fixedToCamera = true;
 
}
function updateHealthBar() {
 
    var m = (100 -player.life*10) / 100;
    var bh = 92 - (92 * m);
    var offset = 92 - bh;
    health.key.context.clearRect(0, 0, health.width, health.height);
    health.key.context.fillRect(offset,0,bh,25);
    health.key.dirty = true;
 
}


function createHealthBar2() {
    meters2 = game.add.group();
 
    // create a plain black rectangle to use as the background of a health meter
    var meterBackgroundBitmap2 = game.add.bitmapData(100, 20);
    meterBackgroundBitmap2.ctx.beginPath();
    meterBackgroundBitmap2.ctx.rect(0, 0, meterBackgroundBitmap2.width, meterBackgroundBitmap2.height);
    meterBackgroundBitmap2.ctx.fillStyle = '#000000';
    meterBackgroundBitmap2.ctx.fill();
 
    // create a Sprite using the background bitmap data
    var healthMeterBG2 = game.add.sprite(95,40, meterBackgroundBitmap2);
    healthMeterBG2.fixedToCamera = true;
    meters.add(healthMeterBG2);
 
    // create a red rectangle to use as the health meter itself
    var healthBitmap2 = game.add.bitmapData(92, 12);
    healthBitmap2.ctx.beginPath();
    healthBitmap2.ctx.rect(0, 0, healthBitmap2.width, healthBitmap2.height);
    healthBitmap2.ctx.fillStyle = '#000079';
    healthBitmap2.ctx.fill();
    // create the health Sprite using the red rectangle bitmap data
    health2 = game.add.sprite(100, 44, healthBitmap2);
    meters.add(health2);
    health2.fixedToCamera = true;
}
function updateHealthBar2() {
    var m2 = (100 -player2.life*10) / 100;
    var bh2 = 92 - (92 * m2);
    var offset2 = 92 - bh2;
    health2.key.context.clearRect(0, 0, health2.width, health2.height);
    health2.key.context.fillRect(offset2,0,bh2,25);
    health2.key.dirty = true;
}