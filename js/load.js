var loadState = {
    preload: function () {
    // Add a 'loading...' label on the screen 
    this.scale.pageAlignHorizontally = true;
    game.load.crossOrigin = 'anonymous';
    var loadingLabel = game.add.text(game.width/2, 350,'loading...', { font: '30px Arial', fill: '#ffffff' });
    loadingLabel.anchor.setTo(0.5, 0.5);
    // Display the progress bar
    var progressBar = game.add.sprite(game.width/2, 400, 'progressBar');
    progressBar.anchor.setTo(0.5, 0.5);
    game.load.setPreloadSprite(progressBar);
    // Load all game assets
    game.load.audio('gamebgm', 'sound/gamebgm.wav');
    game.load.audio('die', 'sound/scream.wav');
    game.load.audio('hurt', 'sound/hurt.wav');
    game.load.image('instruction','assets/instruction.png');   
    game.load.image('pixel', 'assets/pixel.png');
    game.load.image('menupic','assets/menu.png');
    game.load.spritesheet('player','assets/MARIO.png', 32, 54);
    game.load.spritesheet('player2','assets/MARIO2.png', 32, 54);
    game.load.image('wall', 'assets/wall.png');
    game.load.image('ceiling', 'assets/ceiling.png');
    game.load.image('normal', 'assets/normal.png');
    game.load.image('nails', 'assets/nails.png');
    game.load.spritesheet('conveyorRight', 'assets/conveyor_right.png', 96, 16);
    game.load.spritesheet('conveyorLeft', 'assets/conveyor_left.png', 96, 16);
    game.load.spritesheet('trampoline', 'assets/trampoline.png', 96, 22);
    game.load.spritesheet('fake', 'assets/fake.png', 96, 36);
    // Load a new asset that we will use in the menu state
},
    create: function() {
        // Go to the menu state
         game.state.start('menu');
    }, 
}; 