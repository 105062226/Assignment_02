var top10=90;
var state="state";
var boardState={
    preload:function(){
        top10=90;
        state="state";
    },
    create: function(){     
        var chageinBoard=firebase.database().ref('P1LeaderBoard');
        game.add.text(game.width/2-150,5,"1P Top10",{ font: '60px Arial', fill: '#000000' });
        if(state=="state"){
            chageinBoard.orderByChild('score').limitToFirst(10).on('child_added', function(snapshot){
            this.score=snapshot.val().score*(-1);
            this.name=snapshot.val().whoPlay;
            game.add.text(game.width/2-50,top10,this.name,{ font: '20px Arial', fill: '#ffffff' });
            game.add.text(game.width/3-50,top10,"B"+this.score,{ font: '20px Arial', fill: '#ffffff' });
            top10+=40;
            });
        }
        quit= game.add.text(420,400,"Quit", { font: '25px Arial', fill: '#ffffff' });
        quit.anchor.setTo(0.5, 0.5);
        quit.inputEnabled = true;        
        quit.events.onInputDown.add(quitdown, this);
        quit.events.onInputOver.add(over, this);
        quit.events.onInputOut.add(out, this);
    }
}
