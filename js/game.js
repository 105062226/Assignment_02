var player;
var keyboard;
var platforms = [];
var leftWall;
var rightWall;
var ceiling;
var text1;
var text2;
var text3;
var distance = 0;
var emitter;
var state="game";
var gamebgm;
var diesound;
var hurtsound;
var mainState = {
    preload:function() {    
        this.scale.pageAlignHorizontally = true;
        game.load.crossOrigin = 'anonymous';
        gamebgm=game.add.audio('gamebgm');
        gamebgm.loop=true;
        diesound=game.add.audio('die');
        hurtsound=game.add.audio('hurt');
        distance=0;
    },

    create: function() {
        game.stage.backgroundColor = '#3498db'; 
        keyboard = game.input.keyboard.addKeys({
            'enter': Phaser.Keyboard.ENTER,
            'up': Phaser.Keyboard.UP,
            'down': Phaser.Keyboard.DOWN,
            'left': Phaser.Keyboard.LEFT,
            'right': Phaser.Keyboard.RIGHT,
            'w': Phaser.Keyboard.W,
            'a': Phaser.Keyboard.A,
            's': Phaser.Keyboard.S,
            'd': Phaser.Keyboard.D
        });
        gamebgm.play();
        createPlatforms();
        createHealthBar()
        createEmitter();
        createBounders();
        createPlayer();
        createTextsBoard();
    },
    update: function() {
        this.physics.arcade.collide(player, platforms, effect);
        this.physics.arcade.collide(player, [leftWall, rightWall]);
        this.physics.arcade.collide(player, ceiling,checkTouchCeiling);
        checkGameOver();
        updateHealthBar();
        updatePlayer();
        updatePlatforms();
        updateTextsBoard();
        createPlatforms();
    }
};

function createEmitter(){
        emitter = game.add.emitter(422, 320, 15);
        emitter.makeParticles('pixel');
        emitter.setYSpeed(-150, 150);
        emitter.setXSpeed(-150, 150);
        emitter.setScale(2, 0, 2, 0, 700);
        emitter.gravity = 500;
}

function createPlayer() {
    player = game.add.sprite(200, 10, 'player');
    player.direction = 10;
    game.physics.arcade.enable(player);
    player.body.gravity.y = 400;
    player.animations.add('left', [3,4], 8);
    player.animations.add('right', [1,2], 8);
    player.animations.add('flyleft', [7,8], 12);
    player.animations.add('flyright', [5,6], 12);
    player.animations.add('fly', [0], 12);
    player.life = 10;
    player.unbeatableTime = 0;
    player.touchOn = undefined;
}


function createBounders () {
    leftWall = game.add.sprite(0, 0, 'wall');
    game.physics.arcade.enable(leftWall);
    leftWall.body.immovable = true;

    rightWall = game.add.sprite(449, 0, 'wall');
    game.physics.arcade.enable(rightWall);
    rightWall.body.immovable = true;

    ceiling = game.add.sprite(52, 0, 'ceiling');
    game.physics.arcade.enable(ceiling);
    ceiling.body.immovable = true;
}

var lastTime = 0;
function createPlatforms () {
    if(game.time.now > lastTime + 750) {
        lastTime = game.time.now;
        createOnePlatform();
        distance += 1;
    }
}

function createOnePlatform () {

    var platform;
    var x =  Math.random()*(500 - 96 - 40);
    var y = 450;

    if(x<=55) x=55;
    else if(x>=350)x=350;

    var rand = Math.random() * 100;
    if(rand < 20) {
        platform = game.add.sprite(x, y, 'normal');
    } else if (rand < 40) {
        platform = game.add.sprite(x, y, 'nails');
        game.physics.arcade.enable(platform);
        platform.body.setSize(96, 15, 0, 15);
    } else if (rand < 50) {
        platform = game.add.sprite(x, y, 'conveyorLeft');
        platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
        platform.play('scroll');
    } else if (rand < 60) {
        platform = game.add.sprite(x, y, 'conveyorRight');
        platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
        platform.play('scroll');
    } else if (rand < 80) {
        platform = game.add.sprite(x, y, 'trampoline');
        platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
        platform.frame = 3;
    } else {
        platform = game.add.sprite(x, y, 'fake');
        platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
    }

    game.physics.arcade.enable(platform);
    platform.body.immovable = true;
    platforms.push(platform);
    platform.body.checkCollision.down = false;
    platform.body.checkCollision.left = false;
    platform.body.checkCollision.right = false;
}

function createPlayer() {
    player = game.add.sprite(200, 50, 'player');
    player.direction = 10;
    game.physics.arcade.enable(player);
    player.body.gravity.y = 400;
    player.animations.add('left', [3,4],8,true);
    player.animations.add('right', [1,2],8,true);
    player.animations.add('flyleft', [7,8],8,false);
    player.animations.add('flyright', [5,6],8,false);
    player.animations.add('fly', [0],8,true);
    player.life = 10;
    player.unbeatableTime = 0;
    player.touchOn = undefined;
}

function createTextsBoard () {
    var style = {fill: '#ff0000', fontSize: '20px'}
    text1 = game.add.text(55, 15, '', style);
    text2 = game.add.text(410, 10, '', style);
    text3 = game.add.text(55, 40, '', style);
}

function updatePlayer () {
    if(keyboard.left.isDown) {
        player.body.velocity.x = -250;
    } else if(keyboard.right.isDown) {
        player.body.velocity.x = 250;
    } else {
        player.body.velocity.x = 0;
    }
    setPlayerAnimate(player);
}

function setPlayerAnimate(player) {
    var x = player.body.velocity.x;
    var y = player.body.velocity.y;

    if (x < 0 && y > 0) {
        player.animations.play('flyleft');
    }
    if (x > 0 && y > 0) {
        player.animations.play('flyright');
    }
    if (x < 0 && y == 0) {
        player.animations.play('left');
    }
    if (x > 0 && y == 0) {
        player.animations.play('right');
    }
    if (x == 0){
        player.animations.play('fly');
    }
}

function updatePlatforms () {
    for(var i=0; i<platforms.length; i++) {
        var platform = platforms[i];
        platform.body.position.y -= 2;
        if(platform.body.position.y <= -20) {
            platform.destroy();
            platforms.splice(i, 1);
        }
    }
}

function updateTextsBoard () {
    text1.setText('life:');
   
    text2.setText('B' + distance);
}

function effect(player, platform) {
    if(platform.key == 'conveyorRight') {
        conveyorRightEffect(player, platform);
    }
    if(platform.key == 'conveyorLeft') {
        conveyorLeftEffect(player, platform);
    }
    if(platform.key == 'trampoline') {
        trampolineEffect(player, platform);
    }
    if(platform.key == 'nails') {
        nailsEffect(player, platform);
    }
    if(platform.key == 'normal') {
        basicEffect(player, platform);
    }
    if(platform.key == 'fake') {
        fakeEffect(player, platform);
    }
}

function conveyorRightEffect(player, platform) {    
    player.body.x += 2;
}

function conveyorLeftEffect(player, platform) {
    player.body.x -= 2;
}

function trampolineEffect(player, platform) {
    platform.animations.play('jump');
    if(player.life<10)
    {
        player.life+=1;
    }
    
    player.body.velocity.y = -225;
}

function nailsEffect(player, platform) {
    if (player.touchOn !== platform) {
        if(player.life-3<0) player.life=0;
        else {
            player.life -= 3;
            hurtsound.play();
        }
        player.touchOn = platform;
        game.camera.flash(0xff0000, 100);
    }
}

function basicEffect(player, platform) {
    if (player.touchOn !== platform) {
        if(player.life < 10) {
            player.life += 1;    
        }
        player.touchOn = platform;
    }
}

function fakeEffect(player, platform) {
    if(player.touchOn !== platform) {
        platform.animations.play('turn');
        setTimeout(function() {
            platform.body.checkCollision.up = false;
        }, 100);
        player.touchOn = platform;
    }
}

function checkTouchCeiling(player) {
    if(player.life-3<0) player.life=0;
    else {
        player.life-=3;
        hurtsound.play();
    }
    game.camera.flash(0xff0000, 100);
}
var onetime=0
function checkGameOver () {
    if(!player.inWorld||player.life<=0) {
        player.life=0;
        if(game.time.now>time){
            diesound.play();
        }
        time=game.time.now+1000;
        gameOver();
    }
}

function gameOver () {
    platforms.forEach(function(s) {s.destroy()});
    platforms = [];
    player.kill();
    emitter.x = player.x;
    emitter.y = player.y;
    emitter.start(true, 800, null, 15);
    game.camera.shake(0.02, 300);
    gamebgm.destroy();
    addleaderboard();  
    game.time.events.add(800,function() {game.state.start('menu');},this);    
}

function createHealthBar() {
 
    meters = game.add.group();
 
    // create a plain black rectangle to use as the background of a health meter
    var meterBackgroundBitmap = game.add.bitmapData(100, 20);
    meterBackgroundBitmap.ctx.beginPath();
    meterBackgroundBitmap.ctx.rect(0, 0, meterBackgroundBitmap.width, meterBackgroundBitmap.height);
    meterBackgroundBitmap.ctx.fillStyle = '#000000';
    meterBackgroundBitmap.ctx.fill();
 
    // create a Sprite using the background bitmap data
    var healthMeterBG = game.add.sprite(95,15, meterBackgroundBitmap);
    healthMeterBG.fixedToCamera = true;
    meters.add(healthMeterBG);
 
    // create a red rectangle to use as the health meter itself
    var healthBitmap = game.add.bitmapData(92, 12);
    healthBitmap.ctx.beginPath();
    healthBitmap.ctx.rect(0, 0, healthBitmap.width, healthBitmap.height);
    healthBitmap.ctx.fillStyle = '#FF0000';
    healthBitmap.ctx.fill();
    // create the health Sprite using the red rectangle bitmap data
    health = game.add.sprite(100, 19, healthBitmap);
    meters.add(health);
    health.fixedToCamera = true;
 
}
function updateHealthBar() {
 
    var m = (100 -player.life*10) / 100;
    var bh = 92 - (92 * m);
    var offset = 92 - bh;
    health.key.context.clearRect(0, 0, health.width, health.height);
    health.key.context.fillRect(offset,0,bh,25);
    health.key.dirty = true;
 
}

function addleaderboard(){
    newleaderboard.set({
        whoPlay:person,
        score: -distance
    });
}
var game = new Phaser.Game(500, 450, Phaser.AUTO, 'canvas');
game.state.add('boot',bootState);
game.state.add('load',loadState);
game.state.add('menu',menuState);
game.state.add('instruction',instrucState);
game.state.add('leaderboard', boardState);
game.state.add('main',mainState);
game.state.add('main2',main2State);
game.state.start('boot');