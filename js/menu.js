var status="menu";
var oneplayer;
var Howtoplay;
var LeaderBoard;
var twoplayer;
var person="";
var entername=false;
var newleaderboard;
var menuState = {
    create: function() {
    var menupic = game.add.sprite(game.width/2,game.height/2-40,'menupic');
    menupic.scale.setTo(0.55,0.55);
    menupic.anchor.setTo(0.5, 0.5);
    var nameLabel = game.add.text(game.width/2, 30, 'Mario Go Downstairs',{ font: '50px Arial', fill: '#ffffff' });
    nameLabel.anchor.setTo(0.5, 0.5);
        createButton('1p',oneplayer,115);
        createButton('2p',twoplayer,85);  
        createButton('How to play',Howtoplay,55);
        createButton('LeaderBoard',LeaderBoard,25);
        if(!entername){
            person = prompt("Please enter your name", "Your name");}
            if(person==""){
                alert("Enter your name");
                person = prompt("Please enter your name", "Your name");
            }
            newleaderboard=firebase.database().ref("P1LeaderBoard").push();
            entername=true;  
    }
};



function createButton(word,variable,high){
    variable = game.add.text(game.width/2, game.height-high, word, { font: '25px Arial', fill: '#ffffff' });
    variable.anchor.setTo(0.5, 0.5);
    variable.inputEnabled = true;        
    variable.events.onInputDown.add(down, this);
    variable.events.onInputOver.add(over, this);
    variable.events.onInputOut.add(out, this);
}

function out(item){
    item.fill="#ffffff";
   
}
 
function over(item)
{
    item.fill = "#ff0044"; 
}
function down (item) {
    if(item.text=="1p")game.state.start('main');
    else if(item.text=="2p")game.state.start('main2');
    else if(item.text=="How to play") game.state.start('instruction');
    else if(item.text=="LeaderBoard") game.state.start('leaderboard');
}